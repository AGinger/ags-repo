$('.nav-top li a').click(function(event) {
	$('body').addClass('opened');
});

$(function() {

	$('.nav-top ul').on('click', 'li:not(.active) a', function() {
		$(this).parent().addClass('active').siblings().removeClass('active')
			.parents('.wrapper').find('.one-tab').fadeOut(0).eq($(this).parent().index()).fadeIn();
			$('.top-title-in').removeClass('active');

		if($(window).width() < 992) {
			$('header').removeClass('show');
			$('body .bg-plan').remove();

			if($(this).parent().hasClass('hide-mob')) {
				map();
			}
		}
	})

});

$('.top-title-in').click(function(event) {
	$(this).addClass('active');
	$('.nav-top ul li').removeClass('active');
	$('.one-tab').fadeOut(0);
	$('.one-tab.contacts').fadeIn();
});

$('.show-hide').click(function(event) {
	$('header').addClass('show');
	addBg();
});

$('.close-mob').click(function(event) {
	$('header').removeClass('show');
	$('body .bg-plan').remove();
	$('body').removeClass('opened-side-modal');
});

$(document).on('click', '.bg-plan', function(event) {
	$('.header').removeClass('show');
	$('body').removeClass('opened-side-modal');
});


function addBg() {
  if($('body .bg-plan').length){
    $('body .bg-plan').remove();
    $('body').prepend('<div class="bg-plan"></div>');
    $('body').addClass('opened-side-modal');
  }
  else {
    $('body').prepend('<div class="bg-plan"></div>');
    $('body').addClass('opened-side-modal');
  }
}

$(document).on('click', '.bg-plan', function(event) {
  $(this).fadeOut();
  $('header').removeClass('show');
});

$(document).ready(function() {
	if($(window).width() < 992) {
		$('.nav-top li:first-child').addClass('active');
	}
});




		/* Map: This represents the map on the page. */
		




$('.top-title-in').click(function(event) {
	$(this).addClass('active');
	$('.nav-top ul li').removeClass('active');
	$('.one-tab').fadeOut(0);
	$('.one-tab.contacts').fadeIn();

	map();

	 

});

function map() {
	setTimeout(function() {

		mapboxgl.accessToken = "pk.eyJ1Ijoid2hpdGVsYWJlbGRldmVsb3BlcnMiLCJhIjoiY2s0d3pqNzVqMGJrdjNsbXpyMDZxcGVoaiJ9.mPCJPnmlPvXbI39Si-cnBQ";

		var map = new mapboxgl.Map({
		  container: "map",
		  style: "mapbox://styles/whitelabeldevelopers/ck7200umo0w7l1jmvfk9b8xgr",
		  zoom: 15,
		  center: [37.6522,55.7084]
		});

map.on("load", function () {
		map.loadImage("/images/ags-place-marker-f-map-1280.png", function(error, image) {
		      if (error) throw error;
		      map.addImage("custom-marker", image);
		      /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
		      map.addLayer({
		        id: "markers",
		        type: "symbol",
		        /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
		        source: {
		          type: "geojson",
		          data: {
		            type: 'FeatureCollection',
		            features: [
		              {
		                type: 'Feature',
		                properties: {},
		                geometry: {
		                  type: "Point",
		                  coordinates: [37.6523, 55.7085]
		                }
		              }
		            ]
		          }
		        },
		        layout: {
		          "icon-image": "custom-marker",
		        }
		      });
		    });
		});

	}, 500);
}